(function (BackboneCollection, Project) {
    var projects = BackboneCollection.extend({
        // Reference to this collection's model.
		model: Project,

        localStorage: new Backbone.LocalStorage('semrush-projects-backbone'),

        comparator: 'order'
    });
    $.extend(true, App.Collections, { Projects: projects });
}(Backbone.Collection, App.Models.Project));
