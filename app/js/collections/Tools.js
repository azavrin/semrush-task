(function (BackboneCollection, Tool) {
    var tools = BackboneCollection.extend({
        // Reference to this collection's model.
		model: Tool,

        localStorage: new Backbone.LocalStorage('semrush-tools-backbone'),

        favorite: function () {
			return new App.Collections.Tools(this.where({isFavorite: true}));
		},

		comparator: 'order'
    });
    $.extend(true, App.Collections, { Tools: tools });
}(Backbone.Collection, App.Models.Tool));
