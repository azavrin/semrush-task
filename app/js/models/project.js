(function (BackboneModel) {
    var project = BackboneModel.extend({
        defaults: function () {
            return {
                keywords: [],
                project_id: '',
                project_name: '',
                url: ''
            };
        }
    });
    $.extend(true, App.Models, { Project: project });
}(Backbone.Model));
