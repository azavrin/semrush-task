(function (BackboneModel) {
    var tool = BackboneModel.extend({
        defaults: function () {
            return {
                tool_name: '',
                isFavorite: false
            };
        },
        
        checkFavorite: function () {
        	this.set('isFavorite', this.get('tool_name') == app.favoriteTool);
        }
    });
    $.extend(true, App.Models, { Tool: tool });
}(Backbone.Model));
