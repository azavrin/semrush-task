// Set up global application namespace using object literals.
var App = App   || {};
App.Models      || (App.Models = {});
App.Collections || (App.Collections = {});
App.Views       || (App.Views = {});

var app = app   || {};
