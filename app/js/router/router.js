﻿(function (BackboneRouter) {
    router = BackboneRouter.extend({
        routes: {
            "": "projectList",
            "project/:id": "getProject"
        },
        projectList: function () {
            app.appView.render();
        },
        getProject: function (id) {
            app.appView.renderProjectDetails(id);
        }
    });
    $.extend(true, App, { Router: router });
})(Backbone.Router);