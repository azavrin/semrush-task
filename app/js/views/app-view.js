(function (View, ProjectView, ProjectDetailsView, ToolsView, Project) {
    var appView = View.extend({
        el: $("#app"),
        events: {
            "click #add-new-prject": function () { this.create(); },
            "click .close-button": function () { this.close(); },
            "click #only-favorite": function () { 
                localStorage.setObject('onlyFavorite', true);
                this.renderProjects('onlyFavorite'); 
            },
            "click #all": function () { 
                localStorage.setObject('onlyFavorite', false);
                this.renderProjects(); 
            },
            "change #tools": function (event) { 
                localStorage.setObject('favoriteTool', event.target.value);
                this.favoriteToolChangeAll(event); 
            },
        },
        initialize: function () {
            this.inputName = this.$('#projectName');
            this.inputDomain = this.$('#projectDomain');
            this.selectFavoriteTool = this.$('#projectFavoriteTool');
            this.listenTo(this.collection, 'add', function (todo) { this.addOne(todo) });
            this.listenTo(this.collection, 'reset', function () { this.addAll() });
        },
        render: function () {
            this.$('.container-fluid').empty();
            var app_main_view = _.template($("#app-main-view-template").html());
            var el = app_main_view({});
            this.$('.container-fluid').html(el);
            this.collection.fetch({ reset: true });
            this.renderSelect();
            this.renderProjects(localStorage.getObject('onlyFavorite') ? 'onlyFavorite' : undefined);
        },
        renderProjectDetails: function (id) {
            this.collection.fetch({ reset: true });
            var model = this.collection.where({project_id: +id});
            if (model.length > 0){
                model = model[0];
                this.$('.container-fluid').empty();
                var view = new ProjectDetailsView({ model: model });
                var el = view.render().el;
                this.$('.container-fluid').html(el);
            } else {
                this.$('.container-fluid').html("<img src='app/img/404.jpg'></p>");
            }
        },
        renderProjects: function (filter){
            this.$('#projects').empty();
            this.onlyFavorite = filter == 'onlyFavorite';
            this.renderSelectValues();
            this.renderFavoriteButton();
            this.addAll();
        },
        renderSelectValues: function (){
            $("#tools").val(localStorage.getObject('favoriteTool'));
            app.favoriteTool = localStorage.getObject('favoriteTool');
            app.toolsCollection.each(this.favoriteToolChangeOne, this);
        },
        renderFavoriteButton: function(onlyFavorite){
            if (this.onlyFavorite){
                this.$('#all').removeClass('active-element');
                this.$('#only-favorite').addClass('active-element');
            } else {
                this.$('#all').addClass('active-element');
                this.$('#only-favorite').removeClass('active-element');
            }
        },
        renderSelect: function (){
            this.$('#tools').empty();
            var tools_options_template = _.template($("#tools_options_template").html());
            var el = tools_options_template({
                tools: app.toolsCollection.toJSON(),
                labelValue: 'Something'
            });
            this.$('#tools').html(el);
        },
        addOne: function (project) {
            var view = new ProjectView({ model: project });
            var el = view.render().el;
            var toolsCollection = localStorage.getObject('onlyFavorite') ?  app.toolsCollection.favorite() : app.toolsCollection;
            var toolsView = new ToolsView({ collection: toolsCollection });
            var toolsEl = toolsView.render().el;
            $(el).find('.tool-set').append(toolsEl);
            this.$("#projects").append(el);
        },
        addAll: function () {
            this.collection.each(this.addOne, this);
        },
        favoriteToolChangeOne: function (tool) {
            tool.set('isFavorite', tool.get('tool_name') == app.favoriteTool);
        },
        favoriteToolChangeAll: function (event) {
            app.favoriteTool = event.target.value;
            app.toolsCollection.each(this.favoriteToolChangeOne, this);
            this.renderProjects(localStorage.getObject('onlyFavorite') ? 'onlyFavorite' : undefined);
        },
        create: function () {
            if (!this.inputName.val() 
                || !this.inputDomain.val() 
                || !(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/.test(this.inputDomain.val()))) {
                return;
            }

            var model = new Project({
                project_name: this.inputName.val(),
                project_id: this.collection.length,
                url: this.inputDomain.val()
            });

            this.collection.add(model);
            model.save();
            this.close();
        },
        clearInputs: function (){
            this.inputName.val('');
            this.inputDomain.val('');
        },
        close: function (){
            $('#newProjectModal').modal('hide');
            this.clearInputs();
        }
    });
    $.extend(true, App.Views, { AppView: appView });
})(Backbone.View, App.Views.ProjectView, App.Views.ProjectDetailsView, App.Views.ToolsView, App.Models.Project);