(function (View) {
    var toolsView = View.extend({
        tagName: "div",
        template: _.template($('#tools-template').html()),
        render: function () {
            this.$el.html(this.template({ tools: this.collection.toJSON() }));
            return this;
        }
    });
    $.extend(true, App.Views, { ToolsView: toolsView });
})(Backbone.View);