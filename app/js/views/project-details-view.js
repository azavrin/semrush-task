(function (View) {
    var projectDetailsView = View.extend({
        tagName: "div",
        template: _.template($('#project-detail-view-template').html()),
        events: {
            "click .destroy": function () { this.deleteModel(); }
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        deleteModel: function () {
	       this.model.destroy();
           window.location.href = "";
    	}
    });
    $.extend(true, App.Views, { ProjectDetailsView: projectDetailsView });
})(Backbone.View);