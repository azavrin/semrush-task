(function (View) {
    var projectView = View.extend({
        tagName: "section",
        template: _.template($('#project-template').html()),
        events: {
            "click .project_name": function () { this.openProjectDetails(); }
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        openProjectDetails: function () {
            window.location.href = "#/project/" + this.model.get('project_id');
        }
    });
    $.extend(true, App.Views, { ProjectView: projectView });
})(Backbone.View);