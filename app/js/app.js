﻿(function (Projects, Tools, AppView, Router, toolsConfig) {
	
	app.toolsCollection = new Tools(toolsConfig);

    app.projectCollection = new Projects();

    app.appView = new AppView({
        collection: app.projectCollection
    });

    app.router = new Router();

    Backbone.history.start();
})(App.Collections.Projects, App.Collections.Tools, App.Views.AppView, App.Router, toolsConfig);