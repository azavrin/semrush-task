module.exports = function (grunt) {

    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "app/css/style.css": "app/css/style.less",
                }
            }
        },
        codekit: {
            options: {},
            js: {
                files: {
                    'app/js/app-libs-ck.js': 'app/js/libs/app-libs.js'
                }
            },
            coreLibsJs: {
                files: {
                    'app/js/core-libs-ck.js': 'app/js/libs/core-libs.js'
                }
            },
        },
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: ['app/js/core-libs-ck.js', 'app/js/app-libs-ck.js', 'app/js/app.js'],
                dest: 'app/js/main-ck.js',
            }
        },
        clean: ["app/js/core-libs-ck.js", "app/js/app-libs-ck.js"]
        /*uglify: {
            options: {
                mangle: false
            },
            libs: {
                files: {
                    'app/js/main-ck.min.js': ['app/js/main-ck.js']
                }
            }
        },*/
    });

    grunt.loadNpmTasks('grunt-codekit');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('default', ['less','codekit', 'concat', 'clean'/*, 'uglify'*/]);
};